package example.jenkins;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;


@Controller("/")
public class IndexController {


    private Common common;

    public IndexController(Common common) {
        this.common = common;
    }

    @Get("/")
    public String gethome (){
        return "JAVA VERSION 2 LOG : "+common.helloWorld;
    }

    @Get("/error")
    public void error (){
        try {
            throw new Exception("ini sample api error");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
