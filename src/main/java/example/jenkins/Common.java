package example.jenkins;

import io.micronaut.context.annotation.Value;

import javax.inject.Singleton;

@Singleton
public class Common {

    @Value("${common.hello-world:'Failed to load config'}")
    public String helloWorld;

}
